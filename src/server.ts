import app from "./api";

const PORT: Number = 3111;

app.listen(PORT, (): void => console.log(`running on port ${PORT}`));
