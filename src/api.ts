import { PrismaClient } from '@prisma/client'
import express from 'express'
import cors from 'cors'

const prisma = new PrismaClient()
const app = express()

app.use(express.json())
app.use(cors({
    origin: '*'
}));
app.get('/users', async (req, res) => {
    const users = await prisma.user.findMany()
    res.json(users)
})
app.get('/feed', async (req, res) => {
    const posts = await prisma.course.findMany({
        where: { published: true },
        include: { users: true }
    })
    res.json(posts)
})

app.get(`/course/:id`, async (req, res) => {
    const { id } = req.params
    const course = await prisma.course.findUnique({
        where: { id: Number(id) },
    })
    res.json(course)
})

app.post(`/user`, async (req, res) => {
    const result = await prisma.user.create({
        data: { ...req.body },
    })
    res.json(result)
})

app.post(`/course`, async (req, res) => {
    const { title, content, authorEmail, imgSrc } = req.body
    const result = await prisma.course.create({
        data: {
            title,
            content,
            published: true,//false to validate the state by the publish endpoint
            imgSrc: imgSrc,
            users: { connect: { email: authorEmail } },
        },
    })
    res.json(result)
})
app.put('/course/publish/:id', async (req, res) => {
    const { id } = req.params
    const course = await prisma.course.update({
        where: { id: Number(id) },
        data: { published: true },
    })
    res.json(course)
})
app.put('/course/edit/:id', async (req, res) => {
    const { id } = req.params
    const { title, content, authorId, imgSrc } = req.body
    const course = await prisma.course.update({
        where: { id: Number(id) },
        data: { title: title, content: content, authorId: authorId, imgSrc: imgSrc },
    })
    res.json(course)
})
app.delete(`/course/:id`, async (req, res) => {
    const { id } = req.params
    const course = await prisma.course.delete({
        where: { id: Number(id) },
    })
    res.json(course)
})

export default app;
