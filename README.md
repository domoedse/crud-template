# Prerequisitos

node v14.18.1 ó superior
docker versión 20.10.12 ó superior
docker-compose versión 1.29.2 ó superior

# larnu-test
Docker con postgresql DB + nodejs express typescript API REST w/ORM Prisma.

## Getting started

EN WINDOWS:

set COMPOSE_DOCKER_CLI_BUILD=1
set DOCKER_BUILDKIT=1
docker-compose build
docker-compose up

EN Linux

export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1
docker-compose build
docker-compose up

## Archivo .env para conectar con Base de Datos.
Archivo .env debe crearse en directorio raíz.
user: usuario BD en docker-compose file.
pass: password BD en docker-compose file.
dbName: Nompre para la Base de datos PostgreSQL.

DATABASE_URL="postgresql://{user}:{pass}@localhost:5432/{dbName}?schema=public"


## instalación dependencias API.

En directorio raíz, ejecutar:
npm install
ó
yarn

## Migración Base de datos

npx prisma migrate dev

## Inciar API

En directorio raíz, ejecutar:
npm run dev
ó
yarn dev

***
## Creación Usuario principal

En directorio raíz, ejecutar:

npx ts-node createUserAndCourse.ts

***
# Instalación dependencias frontend

En directorio frontend, ejecutar:
npm install
ó
yarn

## Iniciar Front end
En directorio frontend, ejecutar:
npm start
ó
yarn start

## Frontend test
En directorio frontend, ejecutar:
npm run test
ó
yarn test
## API tests
En directorio raíz, ejecutar:
npm run test api.test.ts
ó 
yarn test api.test.ts
