import request from "supertest";

import app from "../src/api";
interface PutProps  {
  id: number;
  title: string;
  content: string;
  authorEmail: string;
  imgSrc: string;
}
describe("Test api.ts", () => {
  let auxData: PutProps = { id: 0, title: '', content:'', authorEmail: 'larnutest@larnu.com', imgSrc:''}
  test("feed response at least empty array", async () => {
    const res = await request(app).get("/feed");
    expect(res.body).toEqual(expect.arrayContaining([]));
    res.body.forEach((element: any, indx: number) => {
      if (indx+1===res.body.length){
          auxData = element
      }
    });
  });
  test("Edit course response 200", async () => {
    const res = await request(app).put(`/course/edit/${auxData.id}`)
    .send({
      title: auxData.title+'-TEST',
      content: auxData.content+'-TEST',
      authorEmail: "larnutest@larnu.com",
      imgSrc: auxData.imgSrc,
    })
    .expect(200);
  }); 
});