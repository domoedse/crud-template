import {
  MantineProvider,
  Text,
  AppShell,
  Header,
  Center
} from '@mantine/core';
import Cursos from './components/Cursos';

export default function App() {
  return (
    <MantineProvider withGlobalStyles withNormalizeCSS theme={{
      colorScheme: 'dark',
      colors: {
        // Add your color
        'deep-blue': ['#E9EDFC', '#C1CCF6', '#99ABF0' /* 7 other shades */],
        // or replace default theme color
        blue: ['#E9EDFC', '#C1CCF6', '#99ABF0' /* 7 other shades */],
      }
    }}>
      <AppShell padding="md" header={
      <Header height={60} p="xs">
        <Center>
          <Text>Larnu Cursos</Text>
        </Center></Header>}>
          <Cursos key={'Cursos'}></Cursos>
      </AppShell>
    </MantineProvider>
  );
}