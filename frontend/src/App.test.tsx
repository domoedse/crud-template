import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import replaceAllInserter from 'string.prototype.replaceall';

replaceAllInserter.shim();
test('renders header title', () => {
  render(<App />);
  const linkElement = screen.getByText(/larnu cursos/i);
  expect(linkElement).toBeInTheDocument();
});
