import React from 'react';
import { render, screen } from '@testing-library/react';
import Cursos from './Cursos';
import replaceAllInserter from 'string.prototype.replaceall';

replaceAllInserter.shim();
test('renders at least agregar curso', () => {
  render(<Cursos />);
  const linkElement = screen.getByText(/agregar curso/i);
  expect(linkElement).toBeInTheDocument();
});
