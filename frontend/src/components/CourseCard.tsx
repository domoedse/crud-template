import { useState } from 'react'
import { IconEraser, IconEditCircle, IconX, IconArrowBack } from '@tabler/icons';
import {
    Card,
    Text,
    Container,
    Image,
    Group,
    Button,
    createStyles,
    Modal,
    Center,
    Tooltip,
    Loader
} from '@mantine/core'
import EditCourse from './CourseEditForm';

const useStyles = createStyles((theme) => ({
    card: {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    },

    section: {
        borderBottom: `1px solid ${theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[3]
            }`,
        paddingLeft: theme.spacing.md,
        paddingRight: theme.spacing.md,
        paddingBottom: theme.spacing.md,
    },

    like: {
        color: theme.colors.red[6],
    },

    label: {
        textTransform: 'uppercase',
        fontSize: theme.fontSizes.xs,
        fontWeight: 700,
    },
}));

interface BadgeCardProps {
    idnt: any;
    image: string;
    title: string;
    country: string;
    content: string;
    renderCourses: Function;
}
export default function CourseCard({ idnt, image, title, content, renderCourses }: BadgeCardProps) {
    const { classes, theme } = useStyles();
    const [opened, setOpened] = useState<boolean>(false);
    const [editOpened, setEditOpened] = useState<boolean>(false);
    const [deleteConfirm, setDeleteConfirm] = useState<boolean>(false);
    const [loadingModal, setLoadingModal ] = useState<boolean>(false);
    const handleModal = (evnt: any) => {
        setOpened(true)
    }
    const onDelete = async () => {
        setLoadingModal(true)
        try {
            const res = await fetch(`http://localhost:3111/course/${idnt}`, {
                method: "delete",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": "token-value",
                }
            });
            if (!res.ok) {
                const message = `An error has occured: ${res.status} - ${res.statusText}`;
                throw new Error(message);
            }
            const resCode = await res.status;
            if(resCode===200){
                resetModals();
            }
        } catch (err: any) {
            console.log(err.message);
        }
    }
    const resetModals = () => {
        setLoadingModal(false)
        setOpened(false)
        setEditOpened(false)
        setDeleteConfirm(false)
        renderCourses((state: Boolean)=>!state)
    }
    return (
        <Container size={300} px="xs"><Card withBorder radius="md" p="md" className={classes.card}>
            <Modal
                opened={opened}
                onClose={() => setOpened(false)}
                title={title}
                centered
                transition={'skew-up'}
                transitionDuration={700}
            >
                <Center>
                    <Image src={image} alt={title} height={180} />
                </Center>
                <Center>{content}</Center>
                <Center>
                    <Tooltip label="Editar Curso">
                        <Button onClick={() => setEditOpened(true)}><IconEditCircle /></Button>
                    </Tooltip>
                    <Tooltip label="Eliminar Curso">
                        <Button onClick={()=>setDeleteConfirm(true)}><IconEraser /></Button>
                    </Tooltip>

                </Center>
            </Modal>
            <Modal
                opened={editOpened}
                onClose={() => setEditOpened(false)}
                title={title}
                centered
                transition={'skew-up'}
                transitionDuration={700}
            >
                <EditCourse title={title} image={image} content={content} idnt={idnt} renderCourses={resetModals}/>
            </Modal>
            <Modal
                opened={deleteConfirm}
                onClose={() => setDeleteConfirm(false)}
                title={'Confirmación'}
                centered
                transition={'skew-up'}
                transitionDuration={700}
            >
                <Card.Section className={classes.section} mt="md">
                    <Group position="apart">
                        <Center>
                            <Text>¿Realmente deseas elminar este curso?</Text>
                        </Center>
                        <Center>
                            <Tooltip label="Cancelar">
                                <Button onClick={() => setDeleteConfirm(false)}><IconArrowBack /></Button>
                            </Tooltip>
                            <Tooltip label="Eliminar Curso">
                                <Button onClick={onDelete}><IconX /></Button>
                            </Tooltip>
                        </Center>

                    </Group>
                </Card.Section>
            </Modal>
            <Modal
                opened={loadingModal}
                onClose={() => setLoadingModal(false)}
                title={'Cargando...'}
                centered
                transition={'skew-up'}
                transitionDuration={700}
            >
                <Center><Loader color="grape" size="xl" variant="bars" /></Center>
                
            </Modal>
            <Card.Section className={classes.section} mt="md">
                <Group position="apart">
                    <Center>
                        <Button radius="md" style={{ flex: 1 }} onClick={handleModal}>

                            <Text size="lg" weight={500}>
                                {title}
                            </Text>
                        </Button>

                    </Center>

                </Group>
            </Card.Section>
            <Card.Section>
                <Button radius="md" style={{ flex: 1, height: 180 }} onClick={handleModal}>
                    <Image src={image} alt={title} height={180} />
                </Button>

            </Card.Section>
        </Card></Container >);
}