import React from 'react';
import { render, screen } from '@testing-library/react';
import AddCourse from './CourseForm';
import replaceAllInserter from 'string.prototype.replaceall';

replaceAllInserter.shim();
test('renders add course form', () => {
  render(<AddCourse renderCourses={()=>{}} modalControl={()=>{}}/>);
  const titleElement = screen.getByText(/crea un curso/i);
  expect(titleElement).toBeInTheDocument();
});
