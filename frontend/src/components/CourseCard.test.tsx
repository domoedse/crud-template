import React from 'react';
import { render, screen } from '@testing-library/react';
import CourseCard from './CourseCard';
import replaceAllInserter from 'string.prototype.replaceall';

replaceAllInserter.shim();
test('renders card title', () => {
  const title='Título Tarjeta'
  render(<CourseCard idnt={1} image={null} title={title} content={'Contenido Tarjeta'} renderCourses={()=>{}} country='Remote'/>);
  const titleElement = screen.getByText(title);
  expect(titleElement).toBeInTheDocument();
});
